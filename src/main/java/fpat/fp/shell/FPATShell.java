package fpat.fp.shell;

import java.io.File;

import fpat.fp.shell.creativetab.FPATShellTab;
import futurepack.client.creative.TabFB_Base;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerAboutToStartEvent;
import net.minecraftforge.fml.common.event.FMLServerStartedEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.event.FMLServerStoppingEvent;


@Mod(modid = FPATShell.modID, name = FPATShell.modName, version = FPATShell.modVersion, dependencies = "required-after:fp;" + "required-after:fpatcore")
public class FPATShell
{
    public static final String modID = "fpatshell";
    public static final String modName = "Futurepack Addon - Shell";
    public static final String modVersion = "Version.version";
	
	@Instance(FPATShell.modID)
	public static FPATShell instance;

//	public static ConfigHandler configHandler;
	
	public FPATShell()
	{

	}
	

	public static TabFB_Base FPTab = new FPATShellTab(CreativeTabs.getNextID(), "FPATShellTab");


	@SidedProxy(modId=FPATShell.modID, clientSide="fpat.fp.shell.FPATShellProxyClient", serverSide="fpat.fp.shell.FPATShellProxyServer")
	public static FPATShellProxyBase proxy;

	@EventHandler
    public void preInit(FMLPreInitializationEvent event) 
    {	
//		File configFile = new File(event.getModConfigurationDirectory(), "fpat/decoration/main.conf");
//		configHandler = new ConfigHandler(configFile);
//		configHandler.addConfigurable(new ConfigurationMain());
    	proxy.preInit(event);
    }
	
	@EventHandler
	public void load(FMLInitializationEvent event)
	{
//		configHandler.reload(true);
		proxy.load(event);
	}
	
	@EventHandler
	public void postInit(FMLPostInitializationEvent event)
	{
		proxy.postInit(event);
	}
	
	@EventHandler
	public void preServerStart(FMLServerAboutToStartEvent event) 
	{
		
	}
	
	@EventHandler
	public void serverStarting(FMLServerStartingEvent event) 
	{		

	}
	
	@EventHandler
	public void serverStarted(FMLServerStartedEvent event) 
	{

	}
	
	
	@EventHandler
	public void serverStopped(FMLServerStoppingEvent event) 
	{

	}
}