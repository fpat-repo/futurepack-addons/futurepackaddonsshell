package fpat.fp.shell.research;

import fpat.fp.shell.FPATShell;
import fpat.fp.shell.research.icons.FPATShellResearchIcons;
import futurepack.api.event.ResearchPageRegisterEvent;
import futurepack.common.item.FPItems;
import futurepack.common.research.ResearchLoader;
import futurepack.common.research.ResearchPage;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@Mod.EventBusSubscriber(modid = FPATShell.modID)
public class FPATShellResearchPage
{

	public static ResearchPage fpatdecoration;
	
	/**
	 * Used in {@link ResearchLoader} to make sure, <i>/fp research reload</i>, works.
	 */
	
	@SubscribeEvent
    public static void init(ResearchPageRegisterEvent event)
	{
		fpatdecoration = new ResearchPage("fpatshell").setIcon(new ItemStack(FPATShellResearchIcons.research_tab_pages, 1, 0));
	};
	
}
