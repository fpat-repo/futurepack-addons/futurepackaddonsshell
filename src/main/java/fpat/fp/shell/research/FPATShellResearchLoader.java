package fpat.fp.shell.research;

import java.io.InputStreamReader;

import futurepack.common.FPLog;
import futurepack.common.research.ResearchLoader;

public class FPATShellResearchLoader
{
	
	public static void init()
	{
		try
		{
			ResearchLoader.instance.addResearchesFromReader(new InputStreamReader(FPATShellResearchLoader.class.getResourceAsStream("research.json")));
		}
		catch(Exception ex)
		{
			FPLog.logger.error("Failed to load research.json for FPATShell!");
			ex.printStackTrace();
		}
	}
}
