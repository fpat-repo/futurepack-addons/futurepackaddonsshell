package fpat.fp.shell.blocks;

import fpat.fp.shell.FPATShell;
import futurepack.common.block.FPBlocks;
import futurepack.depend.api.interfaces.IBlockMetaName;
import futurepack.depend.api.interfaces.IItemMetaSubtypes;
import net.minecraft.block.Block;
import net.minecraft.block.BlockGlowstone;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;

public class FPATBlockhazard extends BlockGlowstone implements IBlockMetaName, IItemMetaSubtypes
{
	static String[] names = new String[]{"white","orange","magenta","light_blue","yellow","lime","pink","gray","silver","cyan","purple","blue","brown","green","red","black"}; 
	
	public FPATBlockhazard(Material p_i45394_1_) 
	{
		super(p_i45394_1_);
		setCreativeTab(FPATShell.FPTab);
		setDefaultState(this.blockState.getBaseState().withProperty(FPBlocks.META(names.length-1), 0));
		setLightLevel(1);
	}
	
	@Override
	public void getSubBlocks(CreativeTabs tab, NonNullList l) 
	{
		for(int i=0;i<names.length;i++)
		{
			l.add(new ItemStack(this,1,i));
		}
	}
	
	@Override
	public int damageDropped(IBlockState state) 
	{
		return getMetaFromState(state);
	}
	
	@Override
	public IBlockState getStateFromMeta(int meta)
    {
        return this.getDefaultState().withProperty(FPBlocks.META(names.length-1), meta);
    }

    @Override
	public int getMetaFromState(IBlockState state)
    {
        return state.getValue(FPBlocks.META(names.length-1));
    }

    @Override
	protected BlockStateContainer createBlockState()
    {
        return new BlockStateContainer(this, new IProperty[] {FPBlocks.META(names.length-1)});
    }
	
	@Override
	public int getMaxMetas()
	{
		return 16;
	}

	@Override
	public String getMetaNameSub(ItemStack is) 
	{
		int i = is.getItemDamage() % EnumDyeColor.values().length;
		return EnumDyeColor.values()[i].getUnlocalizedName();

	}

	@Override
	public String getMetaName(int meta) {
		return this.getUnlocalizedName().substring(5)+"_"+meta;
	}
	
}