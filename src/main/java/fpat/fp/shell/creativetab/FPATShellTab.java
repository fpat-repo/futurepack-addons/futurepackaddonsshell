package fpat.fp.shell.creativetab;

import fpat.fp.shell.research.icons.FPATShellResearchIcons;
import futurepack.client.creative.TabFB_Base;
import futurepack.common.block.FPBlocks;

import net.minecraft.item.ItemStack;

public class FPATShellTab extends TabFB_Base {

	public FPATShellTab(int id, String label) 
	{
		super(id, label);
	}

	
	@Override
	public ItemStack getTabIconItem()
	{
		return new ItemStack(FPATShellResearchIcons.research_tab_pages,1,0);
	}
}
