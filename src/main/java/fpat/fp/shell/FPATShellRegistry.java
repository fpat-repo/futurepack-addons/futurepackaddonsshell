package fpat.fp.shell;

import fpat.fp.shell.blocks.FPATShellBlocks;
import fpat.fp.shell.blocks.FPATShellBlocks;
import fpat.fp.shell.blocks.FPATShellBlocks;
import fpat.fp.shell.blocks.FPATShellBlocks;
import fpat.fp.shell.blocks.FPATShellBlocks;
import fpat.fp.shell.blocks.FPATShellBlocks;
import fpat.fp.shell.research.icons.FPATShellResearchIcons;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@Mod.EventBusSubscriber(modid = FPATShell.modID)
public class FPATShellRegistry {
	
	@SubscribeEvent
    public static void registerBlocks(RegistryEvent.Register<Block> event)
	{
		FPATShellBlocks.register(event);
	}
	
	@SubscribeEvent
    public static void registerItems(RegistryEvent.Register<Item> event)
	{
		event.getRegistry().registerAll(FPATShellBlocks.itemBlocks.toArray(new Item[FPATShellBlocks.itemBlocks.size()]));
		FPATShellBlocks.itemBlocks.clear();
		FPATShellBlocks.itemBlocks = null;
		
		FPATShellResearchIcons.register(event);
	}
	
	@SubscribeEvent
	public static void registerModels(ModelRegistryEvent event)
	{
		try
		{
			FPATShellBlocks.setupPreRendering();
		}
		catch(NoSuchMethodError e){}
		FPATShellBlocks.setupRendering();
		FPATShellResearchIcons.setupRendering(); 
	}
}
